module.exports = {
  pgdbHost:
    process.env.FMS_PGDB_HOST || process.env.PGDB_HOST || "localhost",
  pgdbPort: process.env.FMS_PGDB_PORT || process.env.PGDB_PORT || "5432",
  pgdbIsAuth:
    process.env.FMS_PGDB_IS_AUTH || process.env.PGDB_IS_AUTH || "true",
  pgdbUsername:
    process.env.FMS_PGDB_USERNAME || process.env.PGDB_USERNAME || "postgres",
  pgdbPassword:
    process.env.FMS_PGDB_PASSWORD || process.env.PGDB_PASSWORD || "hts1234",

  pgDbName: process.env.PGDB_NAME || "upload_data",

  appPort: process.env.FMS_API_PORT || 3032,
  appHost: process.env.FMS_API_HOST || "0.0.0.0",

  appEnv: process.env.FMS_API_ENV || "dev",
  appLog: process.env.FMS_API_LOG || "dev",

  accessTokenSecret:
    process.env.FMS_API_ACCESS_TOKEN_SECRET ||
    "5A548FD77040AEA80DB67386CF48C0F228E56620A367712E72444DA6379BF65A",
  refreshTokenSecret:
    process.env.FMS_API_REFRESH_TOKEN_SECRET ||
    "6978EB463E14E65513F0E2FF422D2E597EC5AF48D734A59CC2472933550521DC",

  loginAPIServer:
    process.env.FMS_DS_API ||
    process.env.DS_API ||
    "http://localhost:3002/api/v1",
};
