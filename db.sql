-- public.tests definition

-- Drop table

-- DROP TABLE public.tests;

CREATE TABLE public.tests (
	id int4 NOT NULL DEFAULT nextval('test_id_seq'::regclass),
	"Series_reference" varchar NULL,
	"Period" varchar NULL,
	"Data_value" varchar NULL,
	"Suppressed" varchar NULL,
	"STATUS" varchar NULL,
	"UNITS" varchar NULL,
	"Magnitude" varchar NULL,
	"Subject" varchar NULL,
	"Group" varchar NULL,
	"Series_title_1" varchar NULL,
	"Series_title_2" varchar NULL,
	"Series_title_3" varchar NULL,
	"Series_title_4" varchar NULL,
	"Series_title_5" varchar NULL,
	"createdAt" timestamptz(0) NULL,
	"updatedAt" timestamptz(0) NULL
);