"use strict";
//Import Koa Router
const Router = require("koa-router");
//Import Koa Multer
const Multer = require("@koa/multer");
//Import Koa Body
const KoaBody = require("koa-body");
//Import Koa Body
const KoaBodyParser = require("koa-bodyparser");
const fs = require("fs");
//Import Path
const Path = require("path");
//Import Server Routes
const Upload = require("./upload");
// Import csvtojson Module
const CSVToJSON = require('csvtojson');

//Instantiate Router
const router = new Router({
  prefix: "/api/v1",
});

// router.use(KoaBody({ multipart: true }));
router.use(KoaBodyParser());

// const fileUpload = Multer({storage, limits});

//Test Route
router.get("/status", (ctx, next) => {
  ctx.body = "Running!";
});

//File Upload Route
router.post("/fileupload",
	  //fileUpload.single("file"), //To upload single file
  	//fileUpload.fields([{name: "file1"}, {name: "file2"}]), //To upload multiple files
  	async(ctx, next) => {
      /* Multer*/
      try {
      const storage = Multer.memoryStorage();
      //File validation
      const fileFilter = (req, file, cb) => {
        // reject a file
        if (file.mimetype === "application/octet-stream" || file.mimetype === "text/csv") {
          cb(null, true);
        } else {
          cb(
            // Convert workbook.csv file to JSON array
            // (async () => {
            //   try {
            //     const file = await CSVToJSON().fromFile('C:/Users/HyperThink/Desktop/bulk-upload/server/api/v1/controllers/workbook.csv');
            //     // log the JSON array
            //     console.log(file);
            //   } catch (err) {
            //     console.log(err);
            //   }
            // })()
            new Error(
              JSON.stringify({
                rc: 40,
                rd: "Unsupported file!",
              })
            ),
            false
          );
        }
      };

      const upload = Multer({
        storage: storage,
        limits: {
          // 100 mb
          fileSize: 1024 * 1024 * 10,
        },
        fileFilter: fileFilter,
      }).single("file");

      //File Upload Limits
      const limits = {
        fields: 10,
        fileSize: 500 * 1024,
        files: 1 //Number of files to be upload
      }
      
      await upload(ctx, async () => {
        // const buf = ctx.file.buffer;
        var readStream = fs.createReadStream(ctx.file.buffer);
        console.log(readStream);
        // console.log(JSON.parse(ctx.file.buffer.toString()));
        return;
        console.log(buf);
      })
      } catch (err) {
        console.log(err);
      }
})

//User Routes
router.use("/upload", Upload.routes());
//Export
module.exports = router;
